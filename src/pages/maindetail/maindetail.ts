import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController } from 'ionic-angular';
import { SearchPage } from '../search/search';

import { ViewController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CentralProvider } from '../../providers/central/central';
import { MainservicesProvider } from '../../providers/mainservices/mainservices';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LazyLoadImageModule } from 'ng-lazyload-image';

/**
 * Generated class for the MaindetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-maindetail',
  templateUrl: 'maindetail.html',
})
export class MaindetailPage {
  alldata:any=[]
  slide:any
  page:any
  name:any
  getdata:any=[]
  id:any
  Sliders:any=[]
  maindata:any
  image:any
  details:any="detail"
  fixedtime:any
  defaultimage:any
  accestoken:any
  showNOData = 0

  constructor(public loadingCtrl: LoadingController,public plt:Platform,public storage:Storage,public toastCtrl:ToastController,public mainservice:MainservicesProvider,public cent:CentralProvider,public social :SocialSharing,public navCtrl: NavController, public navParams: NavParams,public ViewCtrl:ViewController) {
  // this.name=this.navParams.get("Name")
  this.cent.status=0
  this.accestoken= localStorage.getItem('adftrmee')
  this.page=this.navParams.get("PageName")
  this.id=this.navParams.get("Item")
  this.image=this.cent.imgUrl
this.defaultimage=this.cent.default

    // this.mainservice.mainOffer(this.accestoken,(data)=> this.mainOfferSuccessCallback(data),(data)=> this.mainOfferFailureCallback(data))

    this.mainservice.mainOfferbyid(this.accestoken,this.id,(data)=> this.mainOfferSuccessCallback1(data),(data)=> this.mainOfferFailureCallback(data))

  }
  mainOfferSuccessCallback(data)
  {
    this.alldata=data

  
   for(var i=0;i<this.alldata.length;i++)
   {
     if(this.alldata[i].id==this.id)
     {
       this.name=this.alldata[i].name;
       this.getdata.push(this.alldata[i])
       console.log(this.getdata)
     }
   }

   for(var j=0;j<this.getdata.length;j++)
   {
     this.Sliders.push(this.getdata[j].image)
     this.slide=this.getdata[j].image

 }


  }

  mainOfferSuccessCallback1(data)
  {
    this.showNOData = 0
    this.alldata=data
    if(this.alldata.length == 0 ){
      this.showNOData = 1
      console.log("this.alldata.length <= 0 ")
    }else{
      this.name=this.alldata[0].name;
      this.getdata.push(this.alldata[0])
  
      this.Sliders.push(this.getdata[0].image)
       this.slide=this.getdata[0].image
    }
console.log("mainOffer all data  : ",this.alldata)
  

  
  //  for(var i=0;i<this.alldata.length;i++)
  //  {
  //    if(this.alldata[i].id==this.id)
  //    {
  //      this.name=this.alldata[i].name;
  //      this.getdata.push(this.alldata[i])
  //      console.log(this.getdata)
  //    }
  //  }

//    for(var j=0;j<this.getdata.length;j++)
//    {
//      this.Sliders.push(this.getdata[j].image)
//      this.slide=this.getdata[j].image

//  }


  }

 
  search()
  {
    this.navCtrl.push(SearchPage)
  }
  mainOfferFailureCallback(data)
  {
this.presentToast()
  }
  shareoffer(disc, dis, img) {
    let loading = this.loadingCtrl.create();
    loading.present()
    setTimeout(() => {
      loading.dismiss();
    }, 2000);
    // + "\n" + dis + " ر.س"
    this.social.share(disc, null, img, "https://aldahayanauto.page.link/product?t=2&id=" + this.id).then(() => {
      console.log("success")
    }).catch(() => {
      console.log("not available")
    });
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'تاكد من اتصالك بالخادم',
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }
  authSuccessCallback(data) {

  
    this.cent.appAccess=data.access_token;
    this.mainservice.mainOffer(this.accestoken,(data)=> this.mainOfferSuccessCallback(data),(data)=> this.mainOfferFailureCallback(data))

  }
  authFailureCallback(data)
  {
    this.presentToast()
  }
 
  ionViewDidLoad()
      {
    
       
      }
      doRefresh(ev)
      {
        this.alldata=[]
        this.getdata=[]
        this.Sliders=[]
        this.mainservice.mainOffer(this.accestoken,(data)=> this.mainOfferSuccessCallback(data),(data)=> this.mainOfferFailureCallback(data))
        ev.complete()
      }
}
