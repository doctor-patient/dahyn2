import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, MenuController, LoadingController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { MainservicesProvider } from '../../providers/mainservices/mainservices';
import { CentralProvider } from '../../providers/central/central';
import { DetailsPage } from '../details/details';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SearchPage } from '../search/search';
import { MaindetailPage } from '../maindetail/maindetail';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@Component({
  selector: 'page-cars',
  templateUrl: 'cars.html',
})
export class CarsPage {
  offers: any = "cars"
  alldata: any = []
  defaultimage: any
  img: any
  show: any = true
  offset: any = 1
  show1: any = true
  s: any
  fixedtime: any
  getdata: any = []
  accestoken: any

  offset2: any = 1
  constructor(public loadingCtrl: LoadingController, public menu: MenuController, public toastctrl: ToastController, public plt: Platform, public storage: Storage, public toastCtrl: ToastController, public navCtrl: NavController, public cent: CentralProvider, public mainservice: MainservicesProvider, public social: SocialSharing, public navParams: NavParams, public ViewCtrl: ViewController) {
    this.accestoken = localStorage.getItem('adftrmee')
    this.cent.status = 0
    this.mainservice.CarOffer(this.accestoken, this.offset, (data) => this.CarOfferSuccessCallback(data), (data) => this.CarOfferFailureCallback(data))
    this.mainservice.mainOffer2(this.accestoken,this.offset2 , (data) => this.mainOfferSuccessCallback(data), (data) => this.mainOfferFailureCallback(data))

    this.defaultimage = this.cent.default
    this.img = this.cent.imgUrl
  }

  openmenu() {
    this.menu.open()
  }
  mainOfferSuccessCallback(data) {
    // data[2].price_after = 55
    // data[2].price_before = 2

    this.getdata = data;
    console.log("main Data : ", data)


    if (this.getdata.length == 0) {
      this.show1 = false
    }
    this.storage.set("mainoffer", this.getdata)
  }
  mainOfferFailureCallback(data) {
    this.storage.get("mainoffer").then((val) => {
      this.getdata = val
    })
    this.presentToast()

  }
  search() {
    this.navCtrl.push(SearchPage)
  }
  CarOfferSuccessCallback(data) {
    this.alldata = data;
    console.log("offers Data : ", data)
    if (this.alldata.length > 0) {
      this.storage.set("offerdata", this.alldata)
      this.show = true
      console.log(JSON.stringify(this.alldata))
    }
    else {
      this.show = false
    }

  }
  opendetails(name, id, carid) {

    this.navCtrl.push(DetailsPage, { PageName: "cars", car: id, Name: name, Item: carid })
  }

  opendetails2(name, id) {
    this.navCtrl.push(MaindetailPage, { PageName: "Main", Item: id, Name: name })
  }
  CarOfferFailureCallback(data) {
    this.storage.get("offerdata").then((val) => {
      this.alldata = val
    })
    // this.presentToast()
  }
  shareoffer2(id, name, dis, dis2, img) {
    let price = dis + "بدلا من " + dis2
    // + "\n" + price + " ر.س"
    let loading = this.loadingCtrl.create();
    loading.present()
    setTimeout(() => {
      loading.dismiss();
    }, 2000);
    this.social.share(name, null, img, "https://aldahayanauto.page.link/product?t=2&id=" + id).then(() => {
      console.log("success")
    }).catch(() => {
      console.log("not available")
    });
  }
  shareoffer(id, name, dis, dis2, img) {
    let price = dis + "بدلا من " + dis2
    //+ "\n" + price + " ر.س"
    let loading = this.loadingCtrl.create();
    loading.present()
    setTimeout(() => {
      loading.dismiss();
    }, 2000);
    this.social.share(name, price, img, "https://aldahayanauto.page.link/product?t=1&id=" + id).then(() => {
      console.log("success")
    }).catch(() => {
      console.log("not available")
    });

  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'تاكد من اتصالك بالخادم',
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }
  doRefresh(refresher) {
this.offset=1
this.offset2=1

    this.mainservice.CarOffer(this.accestoken, this.offset, (data) => this.CarOfferSuccessCallback(data), (data) => this.CarOfferFailureCallback(data))
    this.mainservice.mainOffer2(this.accestoken,this.offset2 ,(data) => this.mainOfferSuccessCallback(data), (data) => this.mainOfferFailureCallback(data))
    refresher.complete();




  }


  doInfinite(ev) {
    setTimeout(() => {

      if (this.offset >= 0) {
        this.offset += 1

        this.mainservice.CarOffer(this.accestoken, this.offset, (data) => {

          console.log(JSON.stringify(data))
          if (data.length > 0) {
            data.forEach(element => {
              this.alldata.push(element)
            });



            ev.complete();
          } else {
            this.offset = -1;
            this.offsetToast();
            ev.complete();
          }
        }, (data) => {

          ev.complete();
        }
        )
      }

      else { ev.complete(); };
    }, 500);
  }

  doInfinite2(ev) {
    setTimeout(() => {

      if (this.offset2 >= 0) {
        this.offset2 += 1

        this.mainservice.mainOffer2(this.accestoken, this.offset2, (data) => {

          console.log(JSON.stringify(data))

          if (data.length > 0) {
            data.forEach(element => {
              this.getdata.push(element)
            });



            ev.complete();
          } else {
            this.offset2 = -1;
            this.offsetToast();
            ev.complete();
          }
        }, (data) => {

          ev.complete();
        }
        )
      }

      else { ev.complete(); };
    }, 500);
  }

  offsetToast() {
    let toast = this.toastctrl.create({
      message: 'لا توجد بيانات اخري ',
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

}
