import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { TabsPage } from '../../pages/tabs/tabs';
import { MaintainancePage } from '../../pages/maintainance/maintainance';
import { MainservicesProvider } from '../../providers/mainservices/mainservices';
import { CarsPage } from '../cars/cars';
import { ListcarsPage } from '../../pages/listcars/listcars';
import {Storage} from '@ionic/storage';
import { CentralProvider } from '../../providers/central/central';;
import { SearchPage } from '../search/search';
import { ModalController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { ChatPage } from '../chat/chat';
import { DetailsPage } from '../details/details';
import { AdvicedetailsPage } from '../advicedetails/advicedetails';
import { MaindetailPage } from '../maindetail/maindetail';
import { BranchesPage } from '../branches/branches';
import { ChatpagePage } from '../chatpage/chatpage';
import { AboutPage } from '../about/about';
import { ToastController } from 'ionic-angular';
import { RefreshTokenInterceptor } from '../../providers/mainservices/refresh-token.interceptor';
import { AlertController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  percent:any=[]
  rootPage:any = TabsPage;
  fixedtime:any;
  devID:any;
  badgeg:any
  pltType:any=0
  accestoken:any
  constructor(public event:Events,public viewctrl:ViewController,public alertctrl:AlertController,public toastCtrl:ToastController, public device: Device,public plt:Platform,public modalCtrl:ModalController,public navCtrl: NavController,public storage:Storage, public cent:CentralProvider,public menue :MenuController,public mainservice:MainservicesProvider) {
    this.accestoken= localStorage.getItem('adftrmee')
    this.cent.status=0

    this.plt.ready().then(()=> {
      this.devID = device.uuid  
      this.cent.DeviceId= device.uuid 

 if (this.plt.is('ios')) {
   this.pltType=1
 }

    //  this.mainservice.financerate(this.accestoken,this.cent.DeviceId,(data)=>this. financesuccess(data),(data)=>this .financefail(data))
    });
  }
  financesuccess(data)
  {
    let arr:any=[]
    this.percent.push(data)

    console.log("financesuccess data : ",data)
   console.log("financesuccess: ", JSON.stringify(this.percent))
   console.log(" data.CountNotifcationNotRead[0].notread : ", data.CountNotifcationNotRead[0].notread)
   this.badgeg = data.CountNotifcationNotRead[0].notread
 this.cent.badge=this.badgeg

  for(var i =0;i<this.percent.length;i++)
  {
    
   this.storage.set('limit',this.percent[i].limit)
   arr=this.percent[i].CountNotifcationNotRead

  }
  console.log("arr :",arr)
  // for(var i =0;i<arr.length;i++)
  // {
  //   this.badgeg=arr[i].notread
   

  // }
  //alert("get  counter val " + this.badgeg)

//   this.badgeg = data.CountNotifcationNotRead[0].notread
//  this.cent.badge=this.badgeg

  }
  financefail(data)
  {
    this.presentToast()
  }
  ionViewDidEnter()
  {
    console.log("enter view")
    if(!(navigator.onLine)) 
    {
     

      let toast = this.toastCtrl.create({
        message: 'تاكد من اتصالك بالانترنت',
        duration: 4000,
        position: 'bottom'
      });
      toast.present();       

    }
    else{
    //  alert("call api for counter")
    setTimeout(() => {
      // loading.dismiss();
      this.mainservice.financerate(this.accestoken,this.cent.DeviceId,(data)=>this. financesuccess(data),(data)=>this .financefail(data))
    }, 2000);
  
    }
    this.event.subscribe("counter", (badge) => {
      this.badgeg = badge;
    //  alert("subscribe  counter val " + this.badgeg)
      this.cent.badge=badge
    });
    this.event.subscribe("openOffers", ()=>{
      this.navCtrl.popToRoot().then(()=>{
        this.navCtrl.parent.select(1)
      })
    })
    this.event.subscribe("openChat", ()=>{
      this.navCtrl.popToRoot().then(()=>{
        this.navCtrl.parent.select(2)
      })
    })
    this.event.subscribe("openPaid", ()=>{
      this.navCtrl.popToRoot().then(()=>{
        this.navCtrl.parent.select(3)
      })
    })
    this.badgeg=this.cent.badge
   
  }
  ionViewDidLoad()
  {
    console.log("load view")
     
  }
  maintainanceorderSuccessCallback(data)
  {

  }
  noti()
  {
    this.navCtrl.push(AboutPage)
  }
  maintainanceorderFailureCallback(data)
  {
    this.presentToast()
  }
  opencarspage()
  {
    this.navCtrl.push(ListcarsPage)
  }
  openmenu()
  {
    this.menue.open()
  }
  authFailureCallback(data) {
  this.presentToast()
  }
  authSuccessCallback(data) {

    this.cent.appAccess=data.access_token;

  }
  search()
  {
    this.navCtrl.push(SearchPage)

  }
  optain()
  {
    this.navCtrl.push(MaintainancePage)

  }
 offers()
  {
    //this.navCtrl.push(CarsPage)
    this.navCtrl.parent.select(1)
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'تاكد من اتصالك بالخادم',
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }
  openparts()
  {
    //this.navCtrl.push(ChatPage)
    this.navCtrl.parent.select(2)
  }
  
}

