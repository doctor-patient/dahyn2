import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController ,ActionSheetController,  PopoverController} from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup ,AbstractControl} from '@angular/forms';
import { MainservicesProvider } from '../../providers/mainservices/mainservices';
import { CentralProvider } from '../../providers/central/central';
import { ModalController } from 'ionic-angular';
import { ChatpagePage } from '../chatpage/chatpage';
import { AlertController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { PdfPopupPage } from '../pdf-popup/pdf-popup';
// import { Socket } from 'ng-socket-io';

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  manufacture_id: any;
  model_id: any;
  val:any
  isenabled:any=true
  data:FormGroup
  namee:any
  start_work:any
  modeel:any
  yeaar:any
  maark:any
  end_work:any
  name:AbstractControl
  chasiss:AbstractControl
  model:AbstractControl
  mark:AbstractControl
  cardate:AbstractControl
  year:AbstractControl
  yeardata:any=[]
  brand_data:any=[]
  brandtype:any
  alldata:any=[]
  butDisabled:any=true
  butDisabled1:any=true
  fixedtime:any
  accestoken:any

  StructureNo 
  phone 
  phonee:any
  structureImg = ""
  structureImgForApi = ""
  saveImg = ""

  constructor(    public popoverCtrl:PopoverController,  public camera: Camera,
    public actionSheetCtrl: ActionSheetController,public toastCtrl:ToastController,public storage:Storage,public menue :MenuController,public toast:ToastController,public Alert:AlertController,public modalCtrl:ModalController,public mainservice:MainservicesProvider,public cent:CentralProvider,public formBuilder:FormBuilder,public viewCtrl:ViewController,public navCtrl: NavController, public navParams: NavParams) {
    this.accestoken= localStorage.getItem('adftrmee')
    this.cent.status=0
    this.storage.get("mark").then(val=>{
      if(!(val==null))
      {
       this.mark.setValue(val)
       this.mainservice.brandtype(0,this.accestoken,this.mark.value,(data) => this.brandtypeSuccessCallback(data),(data) => this.brandtypeFailureCallback(data))

      }
     })
     this.storage.get("model").then(val=>{
      if(!(val==null))
      {
       this.model.setValue(val)
      }
     })
     this.storage.get("year").then(val=>{
      if(!(val==null))
      {
       this.year.setValue(val)
      }
     })
     this.storage.get("phone").then(val=>{
      if(!(val==null))
      {
       this.phone.setValue(val)
      }
     })

     this.storage.get("StructureNo").then(val=>{
      if(!(val==null))
      {
       this.StructureNo.setValue(val)
      }
     })

     this.storage.get("structureImg").then(val=>{
      if(!(val==null))
      {
        if(val){
          this.structureImg = 'data:image/jpeg;base64,' + val;
          this.structureImgForApi = encodeURIComponent(val);
        }
        
     
      }
     })

     


    this.mainservice.manufactureyear(this.accestoken,(data) => this.manufactureyearSuccessCallback(data),(data) => this.manufactureyearFailureCallback(data))

    this.storage.get("name").then(val=>{
    if(!(val==null))
    {
     this.name.setValue(val)
    }
   })
    this.mainservice.brands(0,this.accestoken,(data) => this.brandsSuccessCallback(data),(data) => this.brandsFailureCallback(data))
    this.data = this.formBuilder.group({
      name: ['',Validators.required],
      chasiss: ['',Validators.required,],
      model: ['',Validators.required],
      mark: ['',Validators.required,],
      cardate: ['',Validators.required,],
      year: ['',Validators.required,],
      phone: ['',Validators.required,],
      StructureNo: ['','']


    });
    this.name=this.data.controls['name'];
    this.chasiss=this.data.controls['chasiss'];
    this.model=this.data.controls['model'];
    this.mark=this.data.controls['mark'];
    this.cardate=this.data.controls['cardate'];
    this.year=this.data.controls['year'];
    this.phone=this.data.controls['phone'];
    this.StructureNo = this.data.controls['StructureNo'];

  }

 
  brandsSuccessCallback(data)
  {

    this.brand_data=data
   
   
  }
  chatFailureCallback(data)
  {
  }
  chatSuccessCallback(datuua)
  {
    // this.isenabled=true
    this.storage.set('mark',this.mark.value)
    this.storage.set('model',this.model.value)
    this.storage.set('year',this.year.value)
    this.storage.set('phone',this.phone.value)
    this.storage.set('StructureNo',this.StructureNo.value)
    
    this.storage.set('structureImg',this.saveImg)
    
    console.log("chat 2")
    this.mainservice.branch(0,this.accestoken,(data) => this.branchSuccessCallback(data),(data) => this.branchFailureCallback(data))

  

  }
  branchSuccessCallback(data1)
  {
    this.storage.set("name",this.name.value)

    console.log(JSON.stringify(data1))
    data1.forEach(element => {

      this.start_work=element.start_work
      this.end_work=element.end_work
      
    });
    let data=this.name.value+","+this.cent.DeviceId

    // let mapModal = this.modalCtrl.create(ChatpagePage,{Name:data,Model:this.model.value,Mark:this.mark.value,Year:this.year.value,Start:this.start_work,End:this.end_work,Page:"Chat"});
    // mapModal.onDidDismiss(data=>{
    //  this.isenabled=false

    // })
    // mapModal.present();
    console.log("chat3")
    //debugger
    this.isenabled=false
    this.navCtrl.push(ChatpagePage,{Name:data,Model:this.model.value,Mark:this.mark.value,Year:this.year.value,Start:this.start_work,End:this.end_work,Page:"Chat"});
   

  }
  branchFailureCallback(data)
  {

  }
  openmenu()
{
  this.menue.open()
}
  optionsFn()
  {
        this.mainservice.brandtype(0,this.accestoken,this.mark.value,(data) => this.brandtypeSuccessCallback(data),(data) => this.brandtypeFailureCallback(data))
  }
  cancelorder()
  {
    this.data.reset()
  }
  confirmorder()
  {
    
    if(this.name.value=="" ||    this.year.value=="" || this.mark.value=="" || this.model.value=="" ||  this.phone.value=="" || this.name.value.length < 6 || this.phone.value.length < 14)
    {
      this.presentConfirm()
      if(this.name.value=="")
      {
       
        this.namee='true'
      }
      if(this.year.value=="")
      {
      
        this.yeaar='true'
      }
      if(this.mark.value=="")
      {
        
        this.maark='true'
      }
      if(this.model.value=="")
      {
        
        this.modeel='true'
      }

      if(this.phone.value=="")
{
  this.phonee='true'
}

  
    }
    // else if  (this.name.value.length<= 6){
    //   //this.presentConfirm1()

    // }
    //else if  (this.phone.value.length < 14){
    //   //this.presentConfirm2()

    // }
    else
    {

    // this.isenabled = true
console.log("chat 1")
      this.mainservice.chat(this.accestoken,this.mark.value,this.model.value,this.year.value,this.name.value,this.cent.DeviceId,this.phone.value,this.StructureNo.value,this.structureImgForApi,(data) =>this.chatSuccessCallback(data),(data) =>this.chatFailureCallback(data))
     
    
    }
  }


  delphone()
  {
    this.phonee='false'
  }

  delmodel()
  {
    this.modeel='false'
  }
  delyear()
  {
    this.yeaar='false'
  }
  delname()
  {
    this.namee='false'
  }
  optionsFn1(ev)
  {
    this.maark='false'
  this.model_id=ev.target.value
  this.butDisabled1=false
  }
  optionsFn2(ev)
  {
      this.manufacture_id=ev.target.value
  }
  brandsFailureCallback(data)
  {
// this.presentToast()
  }
  brandtypeSuccessCallback(data)
  {

    this.alldata=data
    this.butDisabled=false
    this.isenabled=false

  }
  manufactureyearSuccessCallback(data)
  {
    this.yeardata=data

  }
 manufactureyearFailureCallback(data)
 {
this.presentToast()
 }
 brandtypeFailureCallback(data)
  {

    this.presentToast()
  }
  presentConfirm() {
    let alert = this.toast.create({
      message: 'من فضلك ادخل البيانات كامله',
     duration:3000,
     position:'bottom'
    });
    alert.present();
  }
  presentConfirm1() {
    let alert = this.toast.create({
      message: 'من فضلك ادخل الاسم كاملا',
      duration:3000,
      position:'bottom'
    });
    alert.present();
  }
  presentConfirm2() {
    let alert = this.toast.create({
      message: "رقم الجوال يجب أن يتكون من ١٤ رقم",
      duration:3000,
      position:'bottom'
    });
    alert.present();
  }


  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'تاكد من اتصالك بالخادم',
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }
  authSuccessCallback(data) {

   
    this.cent.appAccess=data.access_token;
    this.mainservice.brands(0,this.cent.appAccess,(data) => this.brandsSuccessCallback(data),(data) => this.brandsFailureCallback(data))

  
  }
  authFailureCallback(data)
  {
    this.presentToast()
  }
  ionViewDidLoad()
      {
    
      }



      changeTxt(){
        console.log("phone... : ",this.phone.value);
        this.phone.value= this.textArabicNumbersReplacment(this.phone.value);
        console.log("phone after replacement: ",this.phone.value); 
      
      }
    
      textArabicNumbersReplacment(strText) {
        // var strTextFiltered = strText.Trim().replace(" ", "");
       
        var strTextFiltered = strText.trim();
        strTextFiltered = strText;
        // //
        // // strTextFiltered = strTextFiltered.replace('ي', 'ى');
        // strTextFiltered = strTextFiltered.replace(/[\ي]/g, 'ى');
        // // strTextFiltered = strTextFiltered.replace('ئ', 'ى');
        // strTextFiltered = strTextFiltered.replace(/[\ئ]/g, 'ى');
        // //
        // // strTextFiltered = strTextFiltered.replace('أ', 'ا');
        // strTextFiltered = strTextFiltered.replace(/[\أ]/g, 'ا');
        // // strTextFiltered = strTextFiltered.replace('إ', 'ا');
        // strTextFiltered = strTextFiltered.replace(/[\إ]/g, 'ا');
        // // strTextFiltered = strTextFiltered.replace('آ', 'ا');
        // strTextFiltered = strTextFiltered.replace(/[\آ]/g, 'ا');
        // // strTextFiltered = strTextFiltered.replace('ء', 'ا');
        // strTextFiltered = strTextFiltered.replace(/[\ء]/g, 'ا');
        // //كاشيده
        // strTextFiltered = strTextFiltered.replace(/[\u0640]/g, '');
        // // التنوين  Unicode Position              
        // strTextFiltered = strTextFiltered.replace(/[\u064B\u064C\u064D\u064E\u064F\u0650\u0651\u0652]/g, '');
        // // چ
        // strTextFiltered = strTextFiltered.replace(/[\u0686]/g, 'ج');
        // // ڤ
        // strTextFiltered = strTextFiltered.replace(/[\u06A4]/g, 'ف');
        // //                
        // // strTextFiltered = strTextFiltered.replace('ة', 'ه');
        // strTextFiltered = strTextFiltered.replace(/[\ة]/g, 'ه');
        // // strTextFiltered = strTextFiltered.replace('ؤ', 'و');
        // strTextFiltered = strTextFiltered.replace(/[\ؤ]/g, 'و');
        // //
        strTextFiltered = strTextFiltered.replace(/[\٩]/g, '9');
        strTextFiltered = strTextFiltered.replace(/[\٨]/g, '8');
        strTextFiltered = strTextFiltered.replace(/[\٧]/g, '7');
        strTextFiltered = strTextFiltered.replace(/[\٦]/g, '6');
        strTextFiltered = strTextFiltered.replace(/[\٥]/g, '5');
        strTextFiltered = strTextFiltered.replace(/[\٤]/g, '4');
        strTextFiltered = strTextFiltered.replace(/[\٣]/g, '3');
        strTextFiltered = strTextFiltered.replace(/[\٢]/g, '2');
        strTextFiltered = strTextFiltered.replace(/[\١]/g, '1');
        strTextFiltered = strTextFiltered.replace(/[\٠]/g, '0');
        //
        return strTextFiltered;
        //
      }
  
      presentActionSheet() { 
    
        let actionSheet = this.actionSheetCtrl.create({
          title: "اختر مصدر الصورة",
          buttons: [
            {
              text: "تحميل من الألبوم",
              handler: () => {
                this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
       //         this.getPhoto();
              }
            },
            {
              text: "استخدم الكاميرا",
              handler: () => {
                this.takePicture(this.camera.PictureSourceType.CAMERA);
              }
            },
            // {
            //   text: this.translate.instant("cancelTxt"),
            //   role: 'cancel'
            // }
          ]
        });
        actionSheet.present();
      }
    

      public takePicture(sourceType) {
        // Create options for the Camera Dialog
        var options = {
          targetWidth: 600,
          targetHeight: 600,
          quality: 80, //20,40
          destinationType: this.camera.DestinationType.DATA_URL,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          sourceType: sourceType,
          saveToPhotoAlbum: false,
          correctOrientation: true
        };
     
        this.camera.getPicture(options).then((imageData) => {
          // imageData is either a base64 encoded string or a file URI
          // If it's base64:
          //this.presentToast("load image");
          this.saveImg = imageData
          this.structureImg = 'data:image/jpeg;base64,' + imageData;
          this.structureImgForApi = encodeURIComponent(imageData);
          // 'profile_pic_ext','jpeg'

        });

      }




  deletePhoto(){
    console.log("delete photo ");
    this.structureImg = ""
    this.structureImgForApi = ""
    this.storage.remove("structureImg");
   
  }


  openImg(img) {
    let popup = this.popoverCtrl.create(PdfPopupPage,{img:img})
  
    popup.present();
  }

      
}

