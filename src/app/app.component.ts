import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, App, Nav, ToastController, AlertController, ModalController, Events, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { FirebaseMessaging } from '@ionic-native/firebase-messaging';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { NavController } from 'ionic-angular';
import { PaidPage } from '../pages/paid/paid';
import { MaintainancePage } from '../pages/maintainance/maintainance';
import { CarsPage } from '../pages/cars/cars';
import { SocialSharing } from '@ionic-native/social-sharing';
import { AppRate } from '@ionic-native/app-rate';
import { ListcarsPage } from '../pages/listcars/listcars';
import { BranchesPage } from '../pages/branches/branches';
import { AdvicesPage } from '../pages/advices/advices';
import { WarningPage } from '../pages/warning/warning';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { JopPage } from '../pages/jop/jop';
import { LoginPage } from '../pages/login/login';
import * as firebase from 'firebase';
import { CarmaintainPage } from '../pages/carmaintain/carmaintain';
import { SettingPage } from '../pages/setting/setting';
import { ChatPage } from '../pages/chat/chat';
import { Storage } from '@ionic/storage';

import Moment from 'moment'
import { DetailsPage } from '../pages/details/details';
import { MaindetailPage } from '../pages/maindetail/maindetail';
import { AdvicedetailsPage } from '../pages/advicedetails/advicedetails';
import { ChatpagePage } from '../pages/chatpage/chatpage';
import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { CentralProvider } from '../providers/central/central';
import { MainservicesProvider } from '../providers/mainservices/mainservices';
import { SearchPage } from '../pages/search/search';
import { Device } from '@ionic-native/device';
import { FirebaseDynamicLinks } from '@ionic-native/firebase-dynamic-links';
var config = {
  apiKey: "AIzaSyCxvnhDX69U0eJ2jlyBdULbTZ8PTh8Bklw",
  authDomain: "aldhyan-a76f0.firebaseapp.com",
  databaseURL: "https://aldhyan-a76f0.firebaseio.com",
  projectId: "aldhyan-a76f0",
  storageBucket: "aldhyan-a76f0.appspot.com",
  messagingSenderId: "74928871835"
};

declare var cordova: any;
@Component({
  templateUrl: 'app.html',
  providers: [FirebaseDynamicLinks]
})
export class MyApp {
  @ViewChild(Nav) navctrl: Nav;
  @ViewChild("content") nav: NavController
  rootPage: any = TabsPage;
  devID: any
  pltType: any = 0
  accestoken: any
  constructor(private inap: InAppBrowser, private device: Device, private cent: CentralProvider,
    private mainservice: MainservicesProvider, private modalCtrl: ModalController,public loadingCtrl: LoadingController,
    private alertctrl: AlertController, private firebaseMessaging: FirebaseMessaging,
    private toastCtrl: ToastController, private plt: Platform, private storage: Storage,
    private screen: ScreenOrientation, statusBar: StatusBar, splashScreen: SplashScreen,
    private appRate: AppRate, private menue: MenuController, private socialSharing: SocialSharing,public events:Events) {
    plt.setDir('rtl', true)
    this.accestoken = localStorage.getItem('adftrmee')

    this.cent.status = 0
    plt.ready().then(() => {
      statusBar.overlaysWebView(false)
      statusBar.backgroundColorByHexString("#fe280b")
      //statusBar.styleDefault();
      splashScreen.hide();
      this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
      this.devID = device.uuid
      cordova.plugins.firebase.messaging.requestPermission().then(function () {
        console.log("Push messaging is allowed");
      });
      cordova.plugins.firebase.dynamiclinks.onDynamicLink((data) => {
        if (data.hasDeepLink) {
          let link = ""
          if (!plt.is('android')) {
            link = data.deepLink
          }
          else {
            link = data.launchURL
          }
          if (link && link.length > 0) {
          let type = (((((link.split('?'))[1]).split('&'))[0]).split('='))[1]
          let id = (((((link.split('?'))[1]).split('&'))[1]).split('='))[1]
          if (type == "1") {
            this.navctrl.push(DetailsPage, { PageName: "listcars", car: id })
          }
          else if (type == "2") {
            this.navctrl.push(MaindetailPage, { PageName: "listcars", Item: id })
          }
        }


        }
        else {
          let link = ""
          if (!plt.is('android')) {
            link = data.deepLink
          }
          else {
            link = data.launchURL
          }
          if (link && link.length > 0) {

          let type = (((((link.split('?'))[1]).split('&'))[0]).split('='))[1]
          let id = (((((link.split('?'))[1]).split('&'))[1]).split('='))[1]
          if (type == "1") {
            this.navctrl.push(DetailsPage, { PageName: "listcars", car: id })
          }
          else if (type == "2") {
            this.navctrl.push(MaindetailPage, { PageName: "listcars", Item: id })
          }
        }
        }
      });
      cordova.plugins.firebase.dynamiclinks.getDynamicLink().then((data) => {
        if (data) {
          console.log("Read dynamic link data on app start:", data);
          let link = ""
          if (!plt.is('android')) {
            link = data.deepLink
          }
          else {
            link = data.launchURL
          }
          if (link && link.length > 0) {

          let type = (((((link.split('?'))[1]).split('&'))[0]).split('='))[1]
          let id = (((((link.split('?'))[1]).split('&'))[1]).split('='))[1]
          if (type == "1") {
            this.navctrl.push(DetailsPage, { PageName: "listcars", car: id })
          }
          else if (type == "2") {
            this.navctrl.push(MaindetailPage, { PageName: "listcars", Item: id })
          }
        }
        
        } else {
          console.log("App wasn't started from a dynamic link");
        }
      });
      //     firebaseDynamicLinks.onDynamicLink()
      // .subscribe((res: any) => console.log("dynamicLinks suc",res), (error:any) => console.log("dynamicLinks err",error));

      this.cent.DeviceId = device.uuid

      if (!this.plt.is('android')) {
        this.pltType = 1
      }
      this.notification()

    });
    firebase.initializeApp(config);
  }
  ionViewDidLoad() {
    this.plt.ready().then(() => {

      this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
      this.devID = this.device.uuid

      this.cent.DeviceId = this.device.uuid

      if (!this.plt.is('android')) {
        this.pltType = 1
      }
      this.notification()
    });
    firebase.initializeApp(config);
  }
  offers() {
    this.nav.setRoot(TabsPage, { tabIndex: 1 });
    //this.events.publish('openOffers')
    this.menue.close()
  }
  openwarning() {
    this.nav.push(WarningPage)
    this.menue.close()
  }
  opencontactus() {
    this.nav.push(BranchesPage)
    this.menue.close()

  }
  openparts() {
    this.nav.push(CarmaintainPage)
    this.menue.close()
  }
  openchat() {
    this.nav.setRoot(TabsPage, { tabIndex: 2 });
    //this.events.publish('openChat')
    this.menue.close()
  }
  openpaid() {
    this.nav.setRoot(TabsPage, { tabIndex: 3 });
    //this.events.publish('openPaid')
    this.menue.close()
  }
  opensetting() {
    this.nav.push(SettingPage)
    this.menue.close()
  }
  openadvices() {
    this.nav.push(AdvicesPage)
    this.menue.close()

  }
  optain() {
    this.nav.push(MaintainancePage)
    this.menue.close()

  }
  openbranches() {
    this.nav.push(BranchesPage)
    this.menue.close()

  }
  shareapp() {
    let loading = this.loadingCtrl.create();
    loading.present()
    setTimeout(() => {
      loading.dismiss();
    }, 2000);

    if (!this.plt.is('android')) {
      this.socialSharing.share("الضحيان للسيارات", null, null, "https://itunes.apple.com/us/app/%D8%A7%D9%84%D8%B6%D8%AD%D9%8A%D8%A7%D9%86-%D9%84%D9%84%D8%B3%D9%8A%D8%A7%D8%B1%D8%A7%D8%AA/id1421240637?ls=1&mt=8").then(() => {
        console.log("success")
      }).catch(() => {
        console.log("not available")
      });
    }
    else {
      this.socialSharing.share("الضحيان للسيارات", null, null, "https://play.google.com/store/apps/details?id=com.ITRoots.AldahayanAuto&ah=51fJvaVo7chCzf2mS2Fykmh_EBs").then(() => {
        console.log("success")
      }).catch(() => {
        console.log("not available")
      });
    }
  }
  opencarspage() {
    this.nav.push(ListcarsPage)
    this.menue.close()

  }
  openjopp() {
    this.nav.push(JopPage)
    this.menue.close()

  }
  loginpage() {
    this.nav.push(LoginPage)
    this.menue.close()

  }
  apprate() {
    this.plt.ready().then(() => {
      this.appRate.preferences.storeAppURL = {
        ios: '1421240637',
        android: 'market://details?id=com.ITRoots.AldahayanAuto',
      },

        this.appRate.preferences.customLocale = {
          title: 'قيم الضحيان للسيارات',
          message: 'اذا اعجبك تطبيق الضحيان للسيارات , هل تمانع من اخذ دقيقه لتقيمه؟ شكرا لدعمك',
          rateButtonLabel: 'قيم البرنامج الان',
          cancelButtonLabel: 'الغاء',
          laterButtonLabel: 'ذكرني لاحقا'

        },
        this.appRate.navigateToAppStore();
      //this.appRate.promptForRating(true);



    }).catch((err) => {
      console.log(err)
    });
  }
  notification() {

    this.firebaseMessaging.onMessage().subscribe((notification) => {
      console.log("New foreground FCM message: ", notification);
      if (!this.plt.is('android')) {
        if (notification["gcm.message_id"] == localStorage.getItem("gcmNotiMsgID")) {
          return
        }

        localStorage.setItem("gcmNotiMsgID", notification["gcm.message_id"])
        console.log("Handling foreground FCM message: ", notification);
        if (notification["gcm.notification.type"] == "4") {
          if (this.cent.status == 1) {


          }
          else {
            let alert = this.alertctrl.create({
              title: notification.aps.alert.title,
              message: notification.aps.alert.body,
              buttons: [
                {
                  text: 'إلغاء',
                  role: 'cancel',
                  handler: () => {
                    console.log('Cancel');
                  }
                },
                {
                  text: 'عرض',
                  handler: () => {
                    this.navctrl.setRoot(TabsPage).then(() => {
                    let mapModal = this.modalCtrl.create(ChatpagePage, { Name: notification["gcm.notification.name"], Device: notification["gcm.notification.device_id"], Page: "Notification" });
                    mapModal.present();
                    })
                  }
                }]
            });
            alert.present();
          }
        }
        else {
          let alert = this.alertctrl.create({
            title: notification.aps.alert.title,
            message: notification.aps.alert.body,
            buttons: [
              {
                text: 'إلغاء',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel');
                }
              },
              {
                text: 'عرض',
                handler: () => {
                  if (notification["gcm.notification.type"] == "2") {
                    this.navctrl.setRoot(TabsPage).then(() => {
                      this.navctrl.push(DetailsPage, { PageName: "notification", car: notification["gcm.notification.ID"], Type: notification["gcm.notification.type"] })
                      console.log("noti id type" + notification["gcm.notification.ID"] + " " + notification["gcm.notification.type"])
                    })
                  }
                  else if (notification["gcm.notification.type"] == "3") {
                    this.navctrl.setRoot(TabsPage).then(() => {
                      this.navctrl.push(MaindetailPage, { PageName: "notification", Item: notification["gcm.notification.ID"], Type: notification["gcm.notification.type"] })
                      console.log("noti id type" + notification["gcm.notification.ID"] + " " + notification["gcm.notification.type"])
                    })
                  }
                  else if (notification["gcm.notification.type"] == "1") {
                    this.navctrl.setRoot(TabsPage).then(() => {
                      this.navctrl.push(AdvicedetailsPage, { Item: notification["gcm.notification.ID"] })
                      console.log("noti id type" + notification["gcm.notification.ID"] + " " + notification["gcm.notification.type"])
                    })
                  }
                  else if (notification["gcm.notification.type"] == "4") {

                    if (this.cent.status == 1) {

                    }
                    else {
                      this.navctrl.setRoot(TabsPage).then(() => {
                      let mapModal = this.modalCtrl.create(ChatpagePage, { Name: notification["gcm.notification.name"], Device: notification["gcm.notification.device_id"], Page: "Notification" });
                      mapModal.present();
                      });
                    }
                  }
                  else if (notification["gcm.notification.type"] == "5") {
                    this.navctrl.setRoot(TabsPage).then(() => {
                      this.navctrl.push(DetailsPage, { PageName: "listcars", car: notification["gcm.notification.ID"], Type: notification["gcm.notification.type"] })

                      console.log("noti id type" + notification["gcm.notification.ID"] + " " + notification["gcm.notification.type"])
                    })

                  }
                  else if (notification["gcm.notification.type"] == "6" || notification["gcm.notification.type"] == "7" || notification["gcm.notification.type"] == "8" || notification["gcm.notification.type"] == "9" || notification["gcm.notification.type"] == "10") {
                    this.navctrl.setRoot(TabsPage).then(() => {
                      this.navctrl.push(AboutPage)
                    })
                  }

                  else {

                    this.navctrl.setRoot(TabsPage, { tabIndex: 0 });


                  }

                }
              }
            ]
          });
          alert.present();
        }

      }
      else {
        let alert = this.alertctrl.create({
          title: notification.title,
          message: notification.body, //message
          buttons: [
            {
              text: 'إلغاء',
              role: 'cancel',
              handler: () => {
                console.log('Cancel');
              }
            },
            {
              text: 'عرض',
              handler: () => {
                if (notification.type == "2") {
                  this.navctrl.setRoot(TabsPage).then(() => {
                    this.navctrl.push(DetailsPage, { PageName: "notification", car: notification.ID, Type: notification.type })
                  });
                } else if (notification.type == "3") {
                  this.navctrl.setRoot(TabsPage).then(() => {
                    this.navctrl.push(MaindetailPage, { PageName: "notification", Item: notification.ID, Type: notification.type })
                  });
                }

                else if (notification.type == "1") {
                  this.navctrl.setRoot(TabsPage).then(() => {
                    this.navctrl.push(AdvicedetailsPage, { Item: notification.ID, Page: "notification" })
                  });
                }

                else if (notification.type == "4") {


                  if (this.cent.status == 1) {

                  }
                  else {
                    this.navctrl.setRoot(TabsPage).then(() => {
                      let mapModal = this.modalCtrl.create(ChatpagePage, { Name: notification.name, Device: notification.device_id, Page: "Notification" });
                      mapModal.present();
                    });
                  }

                }
                else if (notification.type == "5") {
                  this.navctrl.setRoot(TabsPage).then(() => {
                    this.navctrl.push(DetailsPage, { PageName: "listcars", car: notification.ID, Type: notification.type })
                  });
                }
                else if (notification.type == "6" || notification.type == "7" || notification.type == "8" || notification.type == "9" || notification.type == "10") {
                  this.navctrl.setRoot(TabsPage).then(() => {
                    this.navctrl.push(AboutPage)
                  });
                }
                else {

                  this.navctrl.setRoot(TabsPage, { tabIndex: 0 });


                }
              }
            }
          ]
        });
        alert.present();
      }
    });
    this.firebaseMessaging.onBackgroundMessage().subscribe((notification) => {
      console.log("New background FCM message: ", notification);
      // if (notification["gcm.message_id"] != localStorage.getItem("gcmNotiMsgID") ){
      //   return
      // }
      localStorage.setItem("gcmNotiMsgID", notification["gcm.message_id"])
      console.log("Handling background FCM message: ", notification);

      if (!this.plt.is('android')) {

        if (notification["gcm.notification.type"] == "2") {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(DetailsPage, { PageName: "notification", car: notification["gcm.notification.ID"], Type: notification["gcm.notification.type"] })
            console.log("noti id type" + notification["gcm.notification.ID"] + " " + notification["gcm.notification.type"])
          })
        }
        else if (notification["gcm.notification.type"] == "3") {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(MaindetailPage, { PageName: "notification", Item: notification["gcm.notification.ID"], Type: notification["gcm.notification.type"] })
            console.log("noti id type" + notification["gcm.notification.ID"] + " " + notification["gcm.notification.type"])
          })
        }
        else if (notification["gcm.notification.type"] == "1") {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(AdvicedetailsPage, { Item: notification["gcm.notification.ID"] })
            console.log("noti id type" + notification["gcm.notification.ID"] + " " + notification["gcm.notification.type"])
          })
        }
        else if (notification["gcm.notification.type"] == "4") {
          if (this.cent.status == 1) {

          }
          else {
            this.navctrl.setRoot(TabsPage).then(() => {
              let mapModal = this.modalCtrl.create(ChatpagePage, { Name: notification["gcm.notification.name"], Device: notification["gcm.notification.device_id"], Page: "Notification" });
              mapModal.present();
            })
          }
        }
        else if (notification["gcm.notification.type"] == "5") {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(DetailsPage, { PageName: "listcars", car: notification["gcm.notification.ID"], Type: notification["gcm.notification.type"] })

            console.log("noti id type" + notification["gcm.notification.ID"] + " " + notification["gcm.notification.type"])
          })
        }
        else if (notification["gcm.notification.type"] == "6" || notification["gcm.notification.type"] == "7" || notification["gcm.notification.type"] == "8" || notification["gcm.notification.type"] == "9" || notification["gcm.notification.type"] == "10") {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(AboutPage)
          })
        }

        else {

          this.navctrl.setRoot(TabsPage, { tabIndex: 0 });


        }

      }
      else {
        if (notification.type == "2") {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(DetailsPage, { PageName: "notification", car: notification.ID, Type: notification.type })
          });
        } else if (notification.type == "3") {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(MaindetailPage, { PageName: "notification", Item: notification.ID, Type: notification.type })
          });
        }

        else if (notification.type == "1") {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(AdvicedetailsPage, { Item: notification.ID, Page: "notification" })
          });
        }

        else if (notification.type == "4") {


          if (this.cent.status == 1) {

          }
          else {
            this.navctrl.setRoot(TabsPage).then(() => {
              let mapModal = this.modalCtrl.create(ChatpagePage, { Name: notification.name, Device: notification.device_id, Page: "Notification" });
              mapModal.present();
            });
          }

        }
        else if (notification.type == "5") {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(DetailsPage, { PageName: "listcars", car: notification.ID, Type: notification.type })
          });
        }
        else if (notification.type == "6" || notification.type == "7" || notification.type == "8" || notification.type == "9" || notification.type == "10") {
          this.navctrl.setRoot(TabsPage).then(() => {
            this.navctrl.push(AboutPage)
          });
        }
        else {

          this.navctrl.setRoot(TabsPage, { tabIndex: 0 });


        }
      }

    });
    this.firebaseMessaging.getToken().then((token) => {
      console.log("Got device token: ", token);
      this.cent.regid = token;
      this.mainservice.Notification(this.accestoken, this.devID, token, this.pltType, (data) => this.NotificationSuccessCallback(data), (data) => this.NotificationFailureCallback(data))
    });

  }
  search() {
    let modal = this.modalCtrl.create(SearchPage);
    modal.present();
  }
  NotificationSuccessCallback(data) {

  }
  NotificationFailureCallback(data) {
    this.presentToast()

  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'تاكد من اتصالك بالخادم',
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }
  opentwitter() {

    this.inap.create('https://twitter.com/toyotaaldahayan', '_system', 'location=yes');
    this.menue.close()

  }
  openinsta() {

    this.inap.create('https://www.instagram.com/toyotaaldahayan/', '_system', 'location=yes');
    this.menue.close()

  }
  openfacebook() {

    this.inap.create('https://www.facebook.com/Aldahayan.Auto', '_system', 'location=yes');
    this.menue.close()

  }
}
